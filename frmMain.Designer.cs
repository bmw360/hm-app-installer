﻿namespace HmAppInstaller
{
    partial class frmMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSelFile = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnGetUDID = new System.Windows.Forms.Button();
            this.lblAppFile = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblTip = new System.Windows.Forms.Label();
            this.txtUDID = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBundleName = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(38, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "当前App:";
            // 
            // btnSelFile
            // 
            this.btnSelFile.AutoSize = true;
            this.btnSelFile.Location = new System.Drawing.Point(664, 45);
            this.btnSelFile.Name = "btnSelFile";
            this.btnSelFile.Size = new System.Drawing.Size(88, 47);
            this.btnSelFile.TabIndex = 1;
            this.btnSelFile.Text = "...";
            this.btnSelFile.UseVisualStyleBackColor = true;
            this.btnSelFile.Click += new System.EventHandler(this.btnSelFile_Click);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(347, 286);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(107, 47);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "开始安装";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnGetUDID
            // 
            this.btnGetUDID.AutoSize = true;
            this.btnGetUDID.Location = new System.Drawing.Point(664, 170);
            this.btnGetUDID.Name = "btnGetUDID";
            this.btnGetUDID.Size = new System.Drawing.Size(88, 47);
            this.btnGetUDID.TabIndex = 1;
            this.btnGetUDID.Text = "...";
            this.btnGetUDID.UseVisualStyleBackColor = true;
            this.btnGetUDID.Click += new System.EventHandler(this.btnGetUDID_Click);
            // 
            // lblAppFile
            // 
            this.lblAppFile.AutoSize = true;
            this.lblAppFile.Location = new System.Drawing.Point(124, 45);
            this.lblAppFile.MaximumSize = new System.Drawing.Size(500, 0);
            this.lblAppFile.Name = "lblAppFile";
            this.lblAppFile.Size = new System.Drawing.Size(197, 18);
            this.lblAppFile.TabIndex = 2;
            this.lblAppFile.Text = "请选择要安装的hap文件";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(38, 170);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 18);
            this.label2.TabIndex = 0;
            this.label2.Text = "设备UDID:";
            // 
            // lblTip
            // 
            this.lblTip.AutoSize = true;
            this.lblTip.Location = new System.Drawing.Point(65, 365);
            this.lblTip.MaximumSize = new System.Drawing.Size(500, 0);
            this.lblTip.Name = "lblTip";
            this.lblTip.Size = new System.Drawing.Size(305, 18);
            this.lblTip.TabIndex = 3;
            this.lblTip.Text = "提示:请打开手机调试模式并插入电脑";
            // 
            // txtUDID
            // 
            this.txtUDID.Location = new System.Drawing.Point(127, 170);
            this.txtUDID.Multiline = true;
            this.txtUDID.Name = "txtUDID";
            this.txtUDID.Size = new System.Drawing.Size(509, 68);
            this.txtUDID.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(38, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 18);
            this.label3.TabIndex = 0;
            this.label3.Text = "App包名:";
            // 
            // txtBundleName
            // 
            this.txtBundleName.Location = new System.Drawing.Point(127, 101);
            this.txtBundleName.Name = "txtBundleName";
            this.txtBundleName.Size = new System.Drawing.Size(509, 28);
            this.txtBundleName.TabIndex = 5;
            this.txtBundleName.Text = "com.it360.hmos_player";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.txtBundleName);
            this.Controls.Add(this.txtUDID);
            this.Controls.Add(this.lblTip);
            this.Controls.Add(this.lblAppFile);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnGetUDID);
            this.Controls.Add(this.btnSelFile);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.Text = "鸿蒙App安装器1.0";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSelFile;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnGetUDID;
        private System.Windows.Forms.Label lblAppFile;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblTip;
        private System.Windows.Forms.TextBox txtUDID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBundleName;
    }
}

