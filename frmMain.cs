﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HmAppInstaller
{
    public partial class frmMain : Form
    {
        
        string exe_path = Application.StartupPath + "/toolchains/hdc.exe";
        public frmMain()
        {
            InitializeComponent();
        }

        private void btnSelFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Multiselect = false;//该值确定是否可以选择多个文件
            dialog.Title = "请选择鸿蒙Hap文件";
            dialog.Filter = "鸿蒙文件(*.hap)|*.hap";
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string file = dialog.FileName;
                this.lblAppFile.Text = file;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            //string exe_path = "toolchains/hdc.exe";  // 被调exe
            string sd_path = "/sdcard/6f7490dbd87c4d6f91c626d9281eb84e/";
            if (lblAppFile.Text.Trim() == "")
            {
                MessageBox.Show("请先选择要安装的App文件");
                return;
            }
            if (txtBundleName.Text.Trim() == "")
            {
                MessageBox.Show("请输入App包名");
                return;
            }
            string bname = txtBundleName.Text.Trim();
            string appfile = this.lblAppFile.Text;
            string filename = Path.GetFileName(appfile);
            bool ret = false;
            this.lblTip.Text = "开始安装.";
            string command =  " shell am force-stop " + bname;
            RunCmd2(exe_path, command);
            command = " shell bm uninstall  " + bname;
            RunCmd2(exe_path, command);
            this.lblTip.Text = "正在安装...";
            command = " file send "+ appfile +" "+ sd_path + filename;
            RunCmd2(exe_path,command);
            this.lblTip.Text = "正在安装....";
            command = " shell bm install -p " + sd_path;
            RunCmd2(exe_path,command);
            this.lblTip.Text = "正在安装.....";
            command =  " shell rm -rf " + sd_path.TrimEnd('/');
            RunCmd2(exe_path,command);
            this.lblTip.Text = "正在安装......";
            command =  " shell am start -n '"+ bname + "/"+ bname + ".MainAbilityShellActivity'";
            ret = RunCmd2(exe_path,command);
            if (ret)
            {
                this.lblTip.Text = "安装成功.";
            }
        }

        private void btnGetUDID_Click(object sender, EventArgs e)
        {
            string command = exe_path + " shell bm get -udid";
            
            startCMD(command);
            //string udid = "";
            //this.lblUDID.Text = udid;
        }

        /// <summary>
        /// 调用委托它生成一个进程,解决进程阻塞
        /// </summary>
        /// <param name="command"></param>
        public void startCMD(string command)
        {

            Process p = CreateProcess(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.System), "cmd.exe"), "");
            StringBuilder result = new StringBuilder();

            //这里就是我们用来异步读取输出的二个委托事件   
            //一个是正确的信息，另一个是错误的信息输出   
            p.ErrorDataReceived += new DataReceivedEventHandler(delegate (object sender, DataReceivedEventArgs e)
            {
                //也可以是你自义定的其它处理,比如用console.write打印出来等。ShowErrorInfo(e.Data);   
                //result.AppendLine(e.Data);   //收集所有的出错信息
                if (!string.IsNullOrEmpty(e.Data))
                {
                    this.lblTip.Text += e.Data;
                    this.txtUDID.Text += e.Data;
                    result.AppendLine(e.Data);
                }
            });
            p.OutputDataReceived += new DataReceivedEventHandler(delegate (object sender, DataReceivedEventArgs e)
            {
                //ShowNormalInfo(e.Data);
                if (!string.IsNullOrEmpty(e.Data))
                {
                    txtUDID.Text = e.Data;
                    result.AppendLine(e.Data);
                }
                    
                
            });
            p.Start();

            //这二句别忘了，不然不会触发上面的事件   
            p.BeginErrorReadLine();
            p.BeginOutputReadLine();

            //可以做你要的操作，执行批处理或其它控制台程序   
            //。。。。。。。。。。。   

            //p.StandardInput.WriteLine(input);   
            ///////////////   
            p.StandardInput.WriteLine(command + "&exit");
            p.StandardInput.WriteLine("exit");//最后打入退出命令   

            p.WaitForExit();
            p.Close();
            p.Dispose();
           // return result.ToString();


        }

        /// <summary>   
        /// 生成进程   
        /// </summary>   
        /// <param name="filename"></param>   
        /// <returns></returns>   
        public static Process CreateProcess(string filename, string dir)
        {

            Process p = new Process();//进程   
            p.StartInfo.FileName = filename;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            p.StartInfo.CreateNoWindow = true;
            //下面二句不可少，不然会出错   
            p.StartInfo.RedirectStandardInput = true;
            p.StartInfo.RedirectStandardError = true;
            if (!string.IsNullOrEmpty(dir))
                p.StartInfo.WorkingDirectory = dir;

            return p;

        }

        public bool StartProcess(string runFilePath, params string[] args)
        {
            string s = "";
            foreach (string arg in args)
            {
                s = s + arg + " ";
            }
            s = s.Trim();
            Process process = new Process();//创建进程对象    
            ProcessStartInfo startInfo = new ProcessStartInfo(runFilePath, s); // 括号里是(程序名,参数)
            process.StartInfo = startInfo;
            process.Start();
            return true;
        }

        protected int ProcessInfoReturnValue(string runFilePath)
        {
            try
            {

                //string strExePath = HttpContext.Current.Server.MapPath("~\\SWFTools\\pdf2swf.exe");
                ProcessStartInfo info = new ProcessStartInfo(runFilePath);
                // ProcessStartInfo info = new ProcessStartInfo(strExePath,传给EXE 的参数);
                info.UseShellExecute = false;
                //隐藏exe窗口状态
                info.WindowStyle = ProcessWindowStyle.Hidden;
                //运行exe
                Process proBach = Process.Start(info);
                // 取得EXE运行后的返回值，返回值只能是整型
                int returnValue = proBach.ExitCode;
                return returnValue;
            }
            catch (Exception ex)
            {
                txtUDID.Text = ex.Message;

            }
            return 0;
        }
        public static string execCMD(string command)
        {
            System.Diagnostics.Process pro = new System.Diagnostics.Process();
            pro.StartInfo.FileName = "cmd.exe";
            pro.StartInfo.UseShellExecute = false;
            pro.StartInfo.RedirectStandardError = true;
            pro.StartInfo.RedirectStandardInput = true;
            pro.StartInfo.RedirectStandardOutput = true;
            pro.StartInfo.CreateNoWindow = true;
            //pro.ErrorDataReceived += Pro_ErrorDataReceived;
            pro.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            //pro.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            pro.StartInfo.RedirectStandardInput = true;
            pro.StartInfo.RedirectStandardError = true;

            pro.Start();
            pro.StandardInput.WriteLine(command + "&exit");
            pro.StandardInput.WriteLine("exit");
            pro.StandardInput.AutoFlush = true;

            //获取cmd窗口的输出信息
            string output = pro.StandardOutput.ReadToEnd();
            StreamReader sr = pro.StandardOutput;//获取返回值 
            string line = "";
            int num = 1;
            while ((line = sr.ReadLine()) != null)
            {
                if (line != "")
                {
                    Console.WriteLine(line + " " + num++);
                }
            }
            pro.WaitForExit();//等待程序执行完退出进程
            pro.Close();
            return output;

        }
        /// <summary>
        /// 运行cmd命令
        /// 不显示命令窗口
        /// </summary>
        /// <param name="cmdExe">指定应用程序的完整路径</param>
        /// <param name="cmdStr">执行命令行参数</param>
        public bool RunCmd2(string cmdExe, string cmdStr)
        {
            bool result = false;
            try
            {
                using (Process myPro = new Process())
                {
                    myPro.StartInfo.FileName = "cmd.exe";
                    myPro.StartInfo.UseShellExecute = false;
                    myPro.StartInfo.RedirectStandardInput = true;
                    myPro.StartInfo.RedirectStandardOutput = true;
                    myPro.StartInfo.RedirectStandardError = true;
                    myPro.StartInfo.CreateNoWindow = true;
                    myPro.Start();
                    //如果调用程序路径中有空格时，cmd命令执行失败，可以用双引号括起来 ，在这里两个引号表示一个引号（转义）
                    string str = string.Format(@"""{0}"" {1} {2}", cmdExe, cmdStr, "&exit");

                    myPro.StandardInput.WriteLine(str);
                    myPro.StandardInput.AutoFlush = true;
                    myPro.WaitForExit();

                    result = true;
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                lblTip.Text = ex.Message;
                result = false;
            }
            return result;
        }


    }
}
